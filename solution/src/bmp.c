#include "../include/bmp.h"
#include <stdint.h>

#define PADDING 4
#define TYPE 0x4D42
#define HEADER_SIZE 54
#define INFO_HEADER_SIZE 40
#define RESERVED 0
#define PLANES_NUMBER 1
#define BITS_COUNT 24
#define NO_COMPRESSION 0
#define PPX 0
#define COLORS 0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint8_t get_padding(uint32_t width) {
    return PADDING - (width * sizeof(struct pixel)) % PADDING;
}

enum read_status from_bmp(FILE *file, struct image *img) {
	if (!file) {
		return BMP_INVALID_FILE;
	}
	if (!img) {
		return UNINITIALIZED_MEMORY;
	}
    struct bmp_header header;
    if (fread(&header, HEADER_SIZE, 1, file) != 1) {
        return BMP_READ_HEADER_ERROR;
    }
    *img = create_image(header.biWidth, header.biHeight);
    if (!img->data) {
        return CANNOT_ALLOCATE_MEMORY;
    }
    for (uint32_t y = 0; y < img->height; y++) {
        if (fread(pixel_at(0, y, img), sizeof(struct pixel), img->width, file) != img->width ||
            fseek(file, get_padding(img->width), SEEK_CUR) != 0) {
			free_data(img);
            return BMP_READ_DATA_ERROR;
        }
    }
    return BMP_READ_OK;
}

static struct bmp_header create_bmp_header(uint32_t width, uint32_t height) {
	uint32_t size = (sizeof(struct pixel) * width + get_padding(width)) * height;
    return (struct bmp_header) {
            .bfType = TYPE,
            .bfileSize = HEADER_SIZE + size,
            .bfReserved = RESERVED,
            .bOffBits = HEADER_SIZE,
            .biSize = INFO_HEADER_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = PLANES_NUMBER,
            .biBitCount = BITS_COUNT,
            .biCompression = NO_COMPRESSION,
            .biSizeImage = size,
            .biXPelsPerMeter = PPX,
            .biYPelsPerMeter = PPX,
            .biClrUsed = COLORS,
            .biClrImportant = COLORS
    };
}

enum write_status to_bmp(FILE *file, struct image const *img) {
    if (!file) {
		return UNINITIALIZED_FILE;
	}
	if (!img) {
		return UNINITIALIZED_IMAGE;
	}
	struct bmp_header header = create_bmp_header(img->width, img->height);
    if (fwrite(&header, HEADER_SIZE, 1, file) != 1) {
        return BMP_WRITE_ERROR;
    }
    for (uint32_t y = 0; y < img->height; y++) {
        if (fwrite(pixel_at(0, y, img), sizeof(struct pixel), img->width, file) != img->width) {
            return BMP_WRITE_ERROR;
        }
        for (int i = 0; i < get_padding(img->width); i++) {
            if (putc(0, file) == EOF) {
                return BMP_WRITE_ERROR;
            }
        }
    }
    return BMP_WRITE_OK;
}
