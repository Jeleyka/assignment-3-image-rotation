#include "../include/bmp.h"
#include "../include/messages.h"
#include "../include/transformation.h"
#include <stdio.h>

int main(int argc, char** argv) {
    if (argc != 3) {
        print_error(INPUT_ERROR);
        return 1;
    }

    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        print_error(FILE_OPEN_ERROR);
        return 1;
    }
    struct image image;
    enum read_status read_status = from_bmp(in, &image);
	if (fclose(in) == EOF) {
        free_data(&image);
        print_error(FILE_CLOSE_ERROR);
		return 1;
	}
    if (read_status != BMP_READ_OK) {
        print_error(READ_ERROR);
        return 1;
    }
    print_info(READ_OK);
	
	FILE *out = fopen(argv[2], "wb");
    if (out == NULL) {
        free_data(&image);
        print_error(FILE_OPEN_ERROR);
        return 1;
    }
	
    struct image result = rotate(&image);
    enum write_status write_status = to_bmp(out, &result);
	
	free_data(&image);	
    free_data(&result);
	
	if (write_status == BMP_WRITE_ERROR) {
        print_error(WRITE_ERROR);
		fclose(out);
        return 1;
    }
	
    print_info(WRITE_OK);

	if (fclose(out) == EOF) {
        print_error(FILE_CLOSE_ERROR);
		return 1;
	}

    return 0;
}
