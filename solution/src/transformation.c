#include "../include/transformation.h"

struct image rotate(struct image* const image) {
    struct image result = create_image(image->height, image->width);
    if (!result.data) {
        free_data(&result);
        return result;
    }
    for (uint64_t x = 0; x < image->width; ++x) {
        for (uint64_t y = 0; y < image->height; ++y) {
            *pixel_at(image->height - 1 - y, x, &result) = *pixel_at(x, y, image);
        }
    }
    return result;
}
