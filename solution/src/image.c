#include "../include/image.h"

struct pixel *pixel_at(uint32_t x, uint32_t y, struct image const *img) {
	if (!img) return NULL;
    return img->data + x + img->width * y;
}

struct image create_image(uint64_t width, uint64_t height) {
    struct image image;
    image.data = malloc(width * height * sizeof(struct pixel));
    if (!image.data) {
        image.width = 0;
        image.height = 0;
    } else {
        image.width = width;
        image.height = height;
    }
    return image;
}

void free_data(struct image *img) {
	if (!img) return;
    if (img->data)
	    free(img->data);
    img->data = NULL;
    img->width = 0;
    img->height = 0;
}
