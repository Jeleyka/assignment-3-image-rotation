#include "../include/messages.h"

void print_info(enum info_message msg) {
	fprintf(stdin, "%s\n", messages[msg]);
}

void print_error(enum error_message msg) {
	fprintf(stderr, "%s\n", messages[msg]);
}
