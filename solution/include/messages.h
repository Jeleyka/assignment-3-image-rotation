#ifndef LOCALIZATION_H
#define LOCALIZATION_H

#include <stdio.h>

enum info_message {
    READ_OK = 0,
    WRITE_OK
};

enum error_message {
    READ_ERROR = 2,
    WRITE_ERROR,
    FILE_OPEN_ERROR,
    FILE_CLOSE_ERROR,
	INPUT_ERROR
};

static const char *messages[] = {
        [READ_OK] = "File successfully readed.",
        [WRITE_OK] = "File successfully writed.",
        [READ_ERROR] = "Error with reading file.",
        [WRITE_ERROR] = "Error with writing file.",
        [FILE_OPEN_ERROR] = "Error with open file.",
        [FILE_CLOSE_ERROR] = "Error with close file.",
        [INPUT_ERROR] = "Invalid input. Right: <img_folder> <rotate_img_folder>"
};

void print_info(enum info_message msg);

void print_error(enum error_message msg);

#endif
