#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel *pixel_at(uint32_t x, uint32_t y, struct image const *img);

struct image create_image(uint64_t width, uint64_t height);

void free_data(struct image *img);

#endif
