#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdio.h>

enum read_status {
    BMP_READ_OK = 0,
	BMP_INVALID_FILE,
    BMP_READ_DATA_ERROR,
    BMP_READ_HEADER_ERROR,
	UNINITIALIZED_MEMORY,
    CANNOT_ALLOCATE_MEMORY
};

enum read_status from_bmp(FILE *file, struct image *img);

enum write_status {
    BMP_WRITE_OK = 0,
	UNINITIALIZED_FILE,
	UNINITIALIZED_IMAGE,
    BMP_WRITE_ERROR
};

enum write_status to_bmp(FILE *file, struct image const *img);

#endif
